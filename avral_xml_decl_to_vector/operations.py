#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os

from osgeo import ogr
from osgeo import osr

from avral import avral
from avral.operation import AvralOperation, OperationException
from avral.io.types import *
from avral.io.responce import AvralResponce


class XMLDecltoVector(AvralOperation):
    def __init__(self):
        super(XMLDecltoVector, self).__init__(
            name="xml_decl_to_vector",
            inputs=[
                ("out_format", StringType()),
                ("out_name_id", StringType()),
                ("binding", BoolType()),
                ("src", FileType())
            ],
            outputs=[
                ("result", FileType()),
            ],
        )

    def _do_work(self):
        print('-'*30)
        print('start _do_work')
        
        for arg in self.inputs:
            arg_val = self.getInput(arg)
            print(arg,type(arg_val),arg_val)
        
        if self.getInput("out_format")!='':
            out_format = self.getInput("out_format")
        else:
            out_format = 'gpkg'
            
        dst = "vector.zip"
        cmd = "python " + os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            "xml_decl_to_vector.py"
            )
        cmd = cmd + " " + " ".join((
            "--outformat", "{out_format}",
            "--outid", "{out_name_id}",
            "--binding", "{binding}",
            "--inputfile", "{src}",
            "--outfile", "{dst}"
            ))
        cmd = cmd.format(
            out_format=out_format,
            out_name_id=self.getInput("out_name_id"),
            binding=self.getInput("binding"),
            src=self.getInput("src"),
            dst=dst
            )

        print('-'*30)
        print(cmd)

        os.system(cmd)
        
        print('-'*30)
        if os.path.exists(dst):
            self.setOutput("result", dst)
            print('create file result')
        else:
            print('no file result')

        print('end _do_work')
        return ()
