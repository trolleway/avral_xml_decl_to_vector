#!/usr/bin/env python
# -*- coding: utf-8 -*-
import xml.etree.ElementTree as ET


NS = {'ct': 'http://rosleshoz.gov.ru/xmlns/cTypes',
      'st': 'http://rosleshoz.gov.ru/xmlns/sTypes'}


def replace_ns(xml_data):
    out_data = xml_data.split('\n')
    out_data[1]=' '.join([
        '<forestDeclaration',
        f'xmlns:ct="{NS["ct"]}"',
        f'xmlns:st="{NS["st"]}">'
        ])
    out_data = '\n'.join(out_data)
    return out_data


def parse_head(root):
    number = root.find('number').text
    declBegin = root.find('./header/period/ct:begin',NS).text
    declEnd = root.find('./header/period/ct:end',NS).text
    partner = root.find('./header/partner')
    if partner.find('ct:juridicalPerson',NS):
        litcoDecl = partner.find('./ct:juridicalPerson/ct:name',NS).text
    elif partner.find('ct:physicalPerson',NS):
        litcoDecl = ' '.join(
            u'ФЛ',
            partner.find('*ct:first_name',NS).text,
            partner.find('*ct:last_name',NS).text
            )
    elif partner.find('ct:individualEntrepreneur',NS):
        litcoDecl = ' '.join(
            u'ИП',
            partner.find('*ct:first_name',NS).text,
            partner.find('*ct:last_name',NS).text
            )
    else:
        pass
    head = dict(
        nomDecl=number,
        declBegin=declBegin,
        declEnd=declEnd,
        litcoDecl=litcoDecl)
    return head


def parse_objects(root):
    objects = []
    locationInformation = root.find('./locationInformation')
    for row in locationInformation:
        location = row.find('location')
        explication = row.find('explication')
        binding = row.find('binding')
        first_point = (explication[0].find('longitude').text,
                       explication[0].find('latitude').text)
        location_obj = dict(
            nomObjecta = row.find('objectNumber').text,
            ploshad = row.find('overallArea').text,
            expPloshad = row.find('usageArea').text,
            lesnich = location.find('ct:forestry',NS).get('name'),
            uchLesnich = location.find('ct:subforestry',NS).get('name'),
            urochishe = location.find('ct:tract',NS).get('name',default=''),
            kvartal = location.find('ct:quarter',NS).text,
            vydel = location.find('ct:taxationUnit',NS).text,
            lesoseka = location.find('ct:cuttingArea',NS).text,
            obj_geom = parse_geom(explication,first_point),
            bind_geom = parse_geom(binding,first_point)
            )
        objects.append(location_obj)
        
    return objects


def parse_geom(xmltag,add_point=None):
    geom = []
    for row in xmltag:
        lon = row.find('longitude').text
        lat = row.find('latitude').text
        geom.append((lon,lat))
    if len(geom) and add_point:
        geom.append(add_point)
    return geom


def prepare_data(head,objects,fname=''):
    feats = []
    for objitem in objects:
        feats.append({'attrs':{'fName': str(fname)},'geoms':{}})
        for key, value in head.items():
            feats[-1]['attrs'][key] = value
        for key, value in objitem.items():
            if 'geom' in key:
                points = list((float(point[0]),float(point[1])) for point in value)
                feats[-1]['geoms'][key] = points
            else:
                feats[-1]['attrs'][key] = value
    return feats
