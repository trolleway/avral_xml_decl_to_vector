#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import os
import zipfile
from io import BytesIO

import xml.etree.ElementTree as ET
import fileutils
import rlh_parser
import ogrutils
from pprint import pprint
import sys


def get_args():
    p = argparse.ArgumentParser(description='xml_decl_to_vector')

    p.add_argument(
        '--outformat', type=str, default='gpkg',
        choices=['geojson', 'tab', 'shp', 'gpkg'],
        help='out format'
        )
    p.add_argument(
        '--outid', type=str,
        help='outid'
        )
    p.add_argument(
        '--binding', type=bool, default=False,
        help='binding line'
        )
    p.add_argument(
        '--inputfile', type=str,
        help='input fullfilename xml or zip',
        required=True
        )
    p.add_argument(
        '--outfile', type=str,
        help='out zip fullfilename',
        required=True
        )
    args = p.parse_args()

    return args


def action_aff_names_objs(aff_names_objs):
    
    return
    
    
def process():
    print('-'*30)
    print('start process','\n')
    
    args = get_args()
    feats=[]
    # pprint(vars(args))
    
    if args.outid=='':
        args.outid = None
    fpoly_name = 'poly'
    fline_name = 'line'
    if args.outid:
        fpoly_name = '_'.join((args.outid,fpoly_name))
        fline_name = '_'.join((args.outid,fline_name))
    pprint(vars(args))
    
    if os.path.exists(args.inputfile):
        print('input file exist','True')
    else:
        print('input file exist','False')
        raise FileNotFoundError
    print('out file exist',os.path.exists(args.outfile))
    if os.path.exists(args.outfile):
        os.remove(args.outfile)
        print('->remove')
    cwd = os.getcwd()
    tmp_result_path = fileutils.create_dir(cwd,'result',replace=True)
    print('create/replace: ',tmp_result_path)
    
    ff_names_objs, zff_names_objs = fileutils.get_aff_names_objs(
        args.inputfile,['.xml']
        )
    rzff_names_objs = list(
        filter(
            lambda zff_name_obj: str(
                type(zff_name_obj[0])
                )=="<class 'zipfile.Path'>",
            zff_names_objs
            )
        )
    izff_names_objs = list(
        filter(
            lambda zff_name_obj: str(
                type(zff_name_obj[0])
                )=="<class 'list'>",
            zff_names_objs
            )
        )
    print('-'*10)
    print('Найдено: ',len(ff_names_objs))
    print('В архивах: ',len(rzff_names_objs))
    print('В вложенных архивах: ',len(izff_names_objs))
    print('-'*10)
    # pprint(ff_names_objs)
    # print('-'*10)
    # pprint(zff_names_objs)
    
    # # extract_files(fnames_list)
    # # outfile = create_empty_geo(dest,fformat)
    ff_names_objs.extend(zff_names_objs)
    for f_name_obj in ff_names_objs:
        print('File: ',f_name_obj)
        print('read data...')
        xml_data,ff_name = fileutils.read_afile(f_name_obj,'object')
        # if xml_data:
            # print(xml_data[1000:1100])
        print('parse xml...')
        xml_data = rlh_parser.replace_ns(xml_data)
        root = ET.fromstring(xml_data)
        
        head_dict = rlh_parser.parse_head(root)
        # pprint(head_dict)
        objects = rlh_parser.parse_objects(root)
        # pprint.pprint(objects)
        current_feats = rlh_parser.prepare_data(head_dict,objects,fname=ff_name)
        print('Объектов: ',len(current_feats))
        feats.extend(current_feats)
    
    print('Всего объектов: ',len(feats))

    for out_name in (fpoly_name,fline_name):
        print('create ogr ds in memory...')
        if out_name==fline_name and args.binding==False:
            continue
        else:
            if out_name==fline_name:
                memory_ds = ogrutils.create_mem_ds(
                    feats,out_name,'bind_geom','wkbLineString'
                    )
            elif out_name==fpoly_name:
                memory_ds = ogrutils.create_mem_ds(
                    feats,out_name,'obj_geom','wkbPolygon'
                    )
            print(
                memory_ds.GetLayer(out_name).GetName(),
                'type: ',
                memory_ds.GetLayer(out_name).GetGeomType(),
                'объектов: ',
                memory_ds.GetLayer(out_name).GetFeatureCount()
                )

            out_dst = os.path.join(
                tmp_result_path,
                f'{out_name}.{args.outformat}'
                )
            print('create out dst: ',out_dst) 
            out_ds = ogrutils.export_ds(
                memory_ds,
                out_name,
                out_dst,
                outformat=args.outformat)
            memory_ds = None
            out_ds= None
    print('Write result zip...')
    with zipfile.ZipFile(args.outfile, mode='w') as archive:
        for path in os.listdir(tmp_result_path):
            print(path)
            ff_name = os.path.join(tmp_result_path,path)
            if os.path.isfile(ff_name):
                archive.write(ff_name,arcname=path)
        # print(archive.infolist())
    print('\n','end process',sep=''  )
    return
    
process()
