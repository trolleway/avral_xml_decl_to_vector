#!/usr/bin/env python
# -*- coding: utf-8 -*-

from osgeo import ogr
from osgeo import osr
import json


GTYPES={
    'wkbLineString': ogr.wkbLineString,
    'wkbPolygon': ogr.wkbPolygon
    }
    

DNAMES={
    'geojson': 'GeoJSON',
    'shp': 'ESRI Shapefile',
    'gpkg': 'GPKG',
    'tab': 'MapInfo File'
    }

FTYPES={
    'string': ogr.OFTString,
    'real': ogr.OFTReal,
    'date': ogr.OFTDate
    }
    
    
def create_feat(points,geom_type):
    wkbGeomType = GTYPES[geom_type]
    base_geom = ogr.Geometry(wkbGeomType)
    if geom_type == 'wkbLineString':
        for point in points:
            base_geom.AddPoint(point[0],point[1])
    elif geom_type == 'wkbPolygon':
        ring = ogr.Geometry(ogr.wkbLinearRing)
        for point in points:
            ring.AddPoint(point[0],point[1])
        base_geom.AddGeometry(ring)
    return base_geom


def view_feat(geom,outformat='json'):
    geom = geom.ExportToJson()
    feat = json.loads(feat)
    return feat


def create_mem_ds(feats,layer_name,geomkey,geom_type,srs_epsg=4326,field_dict=None):
    out_driver = ogr.GetDriverByName('MEMORY')
    data_source = out_driver.CreateDataSource('memData')
    # tmp = out_driver.Open('memData',1)
    srs = osr.SpatialReference()
    srs.ImportFromEPSG(srs_epsg)
    layer = data_source.CreateLayer(layer_name, srs, GTYPES[geom_type])
    if field_dict:
        for key,value in field_dict.items():
            field_def = ogr.FieldDefn(key,FTYPES[value])
            layer.CreateField(field_def)
    else:
        for key in feats[0]['attrs']:
            field_def = ogr.FieldDefn(key)#, ogr.OFTString)
            layer.CreateField(field_def)
    for feat in feats:
        points = feat['geoms'][geomkey]
        if len(points):
            feature = ogr.Feature(layer.GetLayerDefn())
            geom = create_feat(points,geom_type)
            feature.SetGeometry(geom)
            for key,value in feat['attrs'].items():
                if field_dict:
                    if key in field_dict:
                        feature.SetField(key,value)       
                else:
                    feature.SetField(key,value)
            layer.CreateFeature(feature)
            feature = None
    return data_source
    

def export_ds(input_ds,layername,out_dest,outformat):
    driver_name = DNAMES[outformat]
    outdriver=ogr.GetDriverByName(driver_name)
    out_data_source = outdriver.CreateDataSource(out_dest)
    out_layer=out_data_source.CopyLayer(input_ds.GetLayer(layername),layername,['ENCODING=utf8'])
    # out_data_source = None
    return out_data_source
