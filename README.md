# avral_xml_decl_to_vector

## **Описание**
Создает файл полигонов объектов(делянок,лесосек) из xml файлов лесной декларации,
опционально второй файл - линии привязки.

## **ЛОКАЛЬНЫЙ СКРИПТ**

### установка
````
cd
git clone https://gitlab.com/gornak49/avral_xml_decl_to_vector.git
````
### запуск
````
cd /avral_xml_decl_to_vector/avral_xml_decl_to_vector
python3 xml_decl_to_vector.py --inputfile ../samples/forestDeclaration.xml --outfile ./result/vector.zip
````
### параметры
Обязательные
- --inputfile относительный или абсолютный путь к файлу .xml или .zip или каталогу (по сути это inputpath)
- --outfile относительный или абсолютный путь к выходному файлу .zip

**примечание**

*При запуске скрипта создается каталог [абсолютный путь файла xml_decl_to_vector.py]/result/
в него записываются не заархивированные выходные файлы
каталог перезаписывается при каждом вызове скрипта.
Zip архив пишется в --outfile, который перезаписывается если уже существует*

Опциональные
- --binding True или False -создавать или нет файл линий привязок, по умолчанию False
- --outid префикс имени файлов внутри архива (по умолчанию - создается файл poly,в случае --binding True файлы poly и line)
- --outformat один из списка [geojson,shp,gpkg,tab] по умолчанию gpkg

### пример запуска с параметрами
`
python3 xml_decl_to_vector.py --inputfile ../samples/any_zip.zip --outfile ../any_zip.zip --binding True --outid any_zip --outformat geojson
`
## **DOCKER**

### установка
````
cd
cd avral_xml_decl_to_vector
docker build -t avral_xml_decl_to_vector:latest .
````
### запуск
`
docker run --rm -t -i -v ${PWD}:/avral_xml_decl_to_vector avral_xml_decl_to_vector:latest  /bin/bash
`

внутри контейнера
````
cd
cd /avral_xml_decl_to_vector
pip install ---no-cache-dir /avral_xml_decl_to_vector
````
`
avral-exec xml_decl_to_vector "''" "''" 0 samples/forestDeclaration.xml vector.zip
`
### параметры
**примечания**
- *при запуске в докере параметры задаются строго позиционно в порядке
--outformat --outid --binding --inputfile --outfile*
- *все параметры обязательно прописать*
- *имя параметра не указывается*
- *для передачи значений по умолчанию для текстовых значений используется "''"*
- *для булевых значений 0 или 1*
- *при указании пути для --inputfile : avral при передаче в python автоматически добавляет корневую директорию /avral_xml_decl_to_vector перед прописанным путем, на входе только zip или xml каталог не допускается*
- *каталог result пишется куда-то в tmp*
- *параметр --outfile всегда равен vector.zip и жестко зашит в operations.py и перезаписывается в корневой директории /avral_xml_decl_to_vector*

### пример запуска с параметрами
`
avral-exec xml_decl_to_vector geojson any_zip 1 samples/any_zip.zip vector.zip
`
