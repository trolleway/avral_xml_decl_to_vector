import os

from setuptools import setup

requires = [
    'avral',
    'argparse',
    'gdal'
]

setup(
    name='avral_xml_decl_to_vector',
    version='0.0.1',
    description='Extract object geometry and attrs from forestLD xml to vector file',
    classifiers=[
        "Programming Language :: Python",
    ],
    author='nextgis',
    author_email='info@nextgis.com',
    url='http://nextgis.com',
    keywords='xml_decl_to_vector',
    packages=['avral_xml_decl_to_vector'],
    include_package_data=True,
    zip_safe=False,
    install_requires=requires,
    entry_points={
        'avral_operations': [
            'xml_decl_to_vector = avral_xml_decl_to_vector.operations:XMLDecltoVector',
        ],
    }
)
